var data_001 = 
[{"Concept":"Low income","Element 1":23,"Element 2":32.6,"Element 3":25.4,"Element 4":19},
{"Concept":"Medium income","Element 1":12,"Element 2":3,"Element 3":57.3,"Element 4":27.7},
{"Concept":"High income","Element 1":4,"Element 2":23.5,"Element 3":26.5,"Element 4":46},
{"Concept":"Very high income","Element 1":23,"Element 2":25,"Element 3":31,"Element 4":21}]


var pref_001 = "$";
var suf_001 = "m";

var concept_001, pos_001, thex_001, they_001, largest_001;
var nameNodes_001 = [];
var isFirefox = typeof InstallTrigger !== 'undefined';
var totalNodes_001 = [];

var colorLib =["#9FBBC1", "#73989D", "#4D7E82", "#316C6D", "#306632", "#4D7C4D"]


var jQInteractives = jQuery.noConflict();
(function( $ ) {
  $(function() {

  	//Gets the names of the segments for each concept
	for (var i in data_001[0]) {
		nameNodes_001.push(i);
	}

  	//Gets the concepts, creates a bar container for each
  	for (var i in data_001) {
		$("#stacked_001").append('<div class="rowBar">'+
				'<div class="concept">'+data_001[i][nameNodes_001[0]]+'</div>'+
				'<div class="complete_bar" id="bar_'+i+'"></div>'+
			'</div>')
	}

	//Found the number of nodes, stores values, founds largest to build proportions from it
	for (var i in data_001) {
		var theTotal_001 = 0;
		for (var j in nameNodes_001) {
			if (j>0) {
				theTotal_001  = theTotal_001+(data_001[i][nameNodes_001[j]]);
			}
		}
		totalNodes_001.push(theTotal_001)
	}
	largest_001 = Math.max.apply(Math, totalNodes_001);

	//Create segments, assign colors
	for (var i in data_001) {	
		for (var j in nameNodes_001) {
			if (j>0) {
				$("#bar_"+i).append('<div class="portion" id="id_'+i+"_"+j+'" style="width:'+(data_001[i][nameNodes_001[j]]*100)/largest_001+'%; background:'+colorLib[j-1]+';"><span>'+data_001[i][nameNodes_001[j]]+'</span></div>')
				
				if ($("#id_"+i+"_"+j).width() < 50 ) {
					$("#id_"+i+"_"+j+" span").css("display", "none") 
				}
			}
		}	
	}

	$("#id_0_1 span").append(suf_001).prepend(pref_001);

	//Creates key
	for (var i in nameNodes_001) {
		if (i>0) {
			$("#key_001").append('<div class="keyel_001">'+
				'<div class="keysq_001" style="background:'+colorLib[i-1]+'"><span>'+nameNodes_001[i]+'</span></div>'+
			'</div>')
		}
	}
	

	//Creates tooltip on hover, different way for Firefox
	if (isFirefox == true) {
		$('.portion').mousemove(function(e){
    		$('#tag_001').remove();
			$(".portion").css("-moz-box-shadow","inset 0 0 0 #000000").css("-webkit-box-shadow","inset 0 0 0 #000000").css("box-shadow","inset 0 0 0 #000000")
			var thei = this.id.split("_")[1];
			var thej = this.id.split("_")[2];
			pos_001 = $("#stacked_001").offset();
			thex_001 = e.pageX - pos_001.left;
			they_001 = e.pageY - pos_001.top;
			if (thex_001 > $("#stacked_001").width() - 50) { thex_001 = $("#stacked_001").width() - 50 };
			if (they_001 > $("#stacked_001").height() - 50) { they_001 = $("#stacked_001").height() - 50 };
						
			for (var i in data_001) {
				$("#id_"+i+"_"+thej).css("-moz-box-shadow","inset 0 0 0px 1px #000000").css("-webkit-box-shadow","inset 0 0 0px 1px #000000").css("box-shadow","inset 0 0 3px #000000")
			}

			$("#stacked_001").append('<div id="tag_001" style="top:'+they_001+'px; left:'+thex_001+'px;"><div class="category">'+nameNodes_001[thej]+'</div><div class="valueNum">'+pref_001+nWC(data_001[thei][nameNodes_001[thej]])+suf_001+'</div></div>')
		})
	} else {
		if(isTouch == true) {
				// Do nothing
		} else {
			$('.portion').mousemove(function(){
    			showTag(this.id)
    		})
		}
		
	}
   		
    $('.portion').mouseout(function(){
    	$('#tag_001').remove();
    })

	function showTag(id){


		$('#tag_001').remove();
		$(".portion").css("-moz-box-shadow","inset 0 0 0 #000000").css("-webkit-box-shadow","inset 0 0 0 #000000").css("box-shadow","inset 0 0 0 #000000")
		var thei = id.split("_")[1];
		var thej = id.split("_")[2];
		pos_001 = $("#stacked_001").offset();
		thex_001 = event.pageX - pos_001.left;
		they_001 = event.pageY - pos_001.top;
		if (thex_001 > $("#stacked_001").width() - 200) { thex_001 = $("#stacked_001").width() - 200 };
		if (they_001 > $("#stacked_001").height() - 200) { they_001 = $("#stacked_001").height() - 100 };
				
		for (var i in data_001) {
			$("#id_"+i+"_"+thej).css("-moz-box-shadow","inset 0 0 0px 1px #000000").css("-webkit-box-shadow","inset 0 0 0px 1px #000000").css("box-shadow","inset 0 0 0px 1px #000000")
		}

		$('.portion').mouseout(function(){
    		$(".portion").css("-moz-box-shadow","none").css("-webkit-box-shadow","none").css("box-shadow","none")
    	})

		$("#stacked_001").append('<div id="tag_001" style="top:'+they_001+'px; left:'+thex_001+'px;"><div class="category">'+nameNodes_001[thej]+'</div><div class="valueNum">'+pref_001+nWC(data_001[thei][nameNodes_001[thej]])+suf_001+'</div></div>')
	}

	//Function that assigns thousand commas to value shown on tooltip
	function nWC(x) {
	    var parts = x.toString().split(".");
	    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	    return parts.join(".");
	}

	function isTouch() {  try {  document.createEvent("TouchEvent");  return true; } catch (e) {  return false;  }  }

  });
})(jQInteractives);