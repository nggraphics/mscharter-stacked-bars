var gulp          = require('gulp');
var browserSync   = require('browser-sync');

var REWRITE_MIDDLEWARE;

function initMiddleware() {

	REWRITE_MIDDLEWARE = rewriteModule.getMiddleware(JSON.parse(fs.readFileSync('middleware.json', 'utf8')));
}

gulp.task('browser-sync', function() {
    browserSync({
        server: {
            baseDir: './',
            middleware: REWRITE_MIDDLEWARE
        },
        ghostMode: false,
        startPath: './',
        files: [
			'./js/*.js',
			'./.tmp/*.js',
			'./js/**/*.js'
		]
    });  
});

